# Site web AGEEI

## Qualités recherchées

### Requis

- Git + Git Flow

### Recherché

- Design web
- Backend
- Frontend
- Bases de données
- Sécurité 

## Pages

### Utilisateurs (Phase 1)

#### Accueil

- Mission de l'AGEEI

#### Anciens examens

- Possibilité de filtrer par cours, par année

#### Charte

Pull la charte à partir de gitlab

#### Événements

- Événements passés et à venir
- Redirection vers la page d'inscription

### Admin (Phase 2)

#### Écran de connexion

- Nom d'utilisateur
- Mot de passe
- Se rappeler de moi

#### Gestion des administrateurs

- Ajouter une adresse courriel, un mot de passe par défaut est généré. Lors de sa prochaine connexion il devra changer de mot de passe.
