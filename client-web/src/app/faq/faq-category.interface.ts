import { FAQInterface } from './faq.interface';

export class FAQCategoryInterface {
  categoryName: string;
  faqs: FAQInterface[];
}
